import peerDepsExternal from 'rollup-plugin-peer-deps-external';
import nodePolyfills from 'rollup-plugin-node-polyfills';
import importCss from 'rollup-plugin-import-css';
import babel from '@rollup/plugin-babel';
import commonjs from "@rollup/plugin-commonjs";
import resolve from "@rollup/plugin-node-resolve";
import json from "@rollup/plugin-json";

const packageJson = require("./package.json");

export default {
    input: "src/index.js",
    output: [
        {
            file: packageJson.main,
            format: "amd",
            sourcemap: true,
        }
    ],
    plugins: [
        babel(),
        json(),
        importCss(),
        peerDepsExternal(),
        commonjs(),
        nodePolyfills(),
        resolve(),
    ],
}
