import React from 'react';
import { object, boolean } from '@storybook/addon-knobs';
import ReportContainer from './ReportContainer';

export default { title: 'Views/ReportContainer' };

export const Default = () => (
  <ReportContainer
    open={boolean('Open', true)}
  />
);
