import React, {useContext} from "react";
import {WidgetView} from '@mwp/frontend-components';
import {DemoContext} from "./DemoContext";
import {Box, Button} from "@material-ui/core";

const Widget1 = () => {
    const {count} = useContext(DemoContext);
    return (
        <WidgetView title='Demo plugin'>
            <Box p={1}>
                <h1>{`Current count ${count}`}</h1>
            </Box>
        </WidgetView>
    );
}

const Widget2 = () => {
    const {count, increase} = useContext(DemoContext);
    return (
        <WidgetView title='Demo plugin'>
            <Box p={1}>
                <h1>{`Current count ${count}`}</h1>
            </Box>
            <Button onClick={increase}>Increase</Button>
        </WidgetView>
    );
}

export {
    Widget1,
    Widget2,
};