import React from 'react';
import { object, boolean } from '@storybook/addon-knobs';
import ReportsView from './ReportsView';

export default { title: 'Views/ReportsView' };

export const Default = () => (
  <ReportsView
    open={boolean('Open', true)}
    reports={object('Reports', [
      {
        "id":265,
        "groupId":3,
        "username":"user1",
        "receivedTime":1616674079312,
        "reportTime":1616674078830,
        "category":2,
        "title":"category1",
        "dataText":"",
        "text":"Other",
        "emailDestinations":null,
        "mtLocationId":0,
        "longitude":33,
        "latitude":33,
        "accuracy":2,
        "attachments":[
        ],
        "additionalFieldData":{
        },
        "assetId":0,
        "assetNewState":null,
        "assetNewCondition":null
        },
        {
        "id":264,
        "groupId":3,
        "username":"user1",
        "receivedTime":1616673147016,
        "reportTime":1616673146449,
        "category":2,
        "title":"category1",
        "dataText":"",
        "text":"Ttt",
        "emailDestinations":null,
        "mtLocationId":0,
        "longitude":null,
        "latitude":null,
        "accuracy":null,
        "attachments":[
        ],
        "additionalFieldData":{
        "field1":"one",
        "field2":"two"
        },
        "assetId":0,
        "assetNewState":null,
        "assetNewCondition":null
        },
    ])
  }

  />
);
