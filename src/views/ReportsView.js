/* eslint-disable max-len */
import React from 'react';
import moment from 'moment';
import styled from 'styled-components';
import { Swipeable } from 'react-swipeable';

import { useMediaQuery, useTheme, IconButton, Paper } from '@material-ui/core';
import CloseIcon from '@material-ui/icons/Close';

import { Table, SplitView, Map } from '@mwp/frontend-components';
import ReportDetails from './ReportDetails';

const StyledReportsView = styled.div`
  height: 100%;
  width: 100%;

  .MuiPaper-root {
    height: 100%;
    overflow: hidden;
    display: flex;
    flex-direction: column;
  }

  .reportDetailsWrapper {
    height: 100%;
    position: relative;

    .reportDetails {
      height: 100%;
      position: relative;
      &:before{
        content: "";
        position: absolute;
        height: 4px;
        top: calc(50% - 2px);
        width: 4px;
        background-color: rgba(0,0,0,0.2);
        left: 6px;
        border-radius: 10px;
        box-shadow: 0 -8px 0px rgba(0,0,0,0.2), 0 8px 0px rgba(0,0,0,0.2);
      }
    }
  }

  .closeDetailsBtn {
    position: absolute;
    right: 0px;
    top: 0px;
    z-index:2;
  }
  .reportsTable {
    height: 100%;
    overflow: auto;
    flex-grow: 1;
  }
`;

const ReportsView = ({
    reports
}) => {


  const [selectedReport, setSelectedReport] = React.useState();

  const theme = useTheme();
  const mqXs = 1;/*useMediaQuery(theme.breakpoints.down('xs'));*/
  const [detailsSplitRatio, setDetailsSplitRatio] = React.useState('100% 0%');
  const [tableSplitRatio, setTableSplitRatio] = React.useState(
    mqXs ? '43% 60%' : '33% 66%',
  );

  React.useEffect(() => {
    if (selectedReport) {
      setDetailsSplitRatio(mqXs ? '0%,100%' : '40%,60%');
    } else {
      setDetailsSplitRatio('100%,0%');
    }

    console.log("hey");
    setTableSplitRatio(mqXs ? '43%,60%' : '63%,66%');
  }, [mqXs, selectedReport]);

  const mapProps = {
    center: {
      lat: 51.429573,
      lng: -0.34625,
    },
    zoom: 7,
  };

  const [animateResize, setAnimateResize] = React.useState(true);

  function swiping(e) {
    if (e.first) {
      setAnimateResize(false);
    }
    if (e.deltaX < 0) {
      const x = e.absX;
      viewsRefs[0].current.style = `width:${x}px;`;
      viewsRefs[1].current.style = `width: calc(100% - ${x}px);`;
    } else {
      viewsRefs[0].current.style = 'width: 0px;';
      viewsRefs[1].current.style = 'width: 100%;';
    }
  }

  const viewsRefs = [];

  const getReportsLocations = () => reports
    .map((report, idx) => ({ ...report, idx }))
    .filter((report) => report.latitude)
    .map((report) => ({
      lat: report.latitude,
      lng: report.longitude,
      label: report.name,
      colour: 'green',
      selectedColour: 'red',
      metadata: {
        ...report,
        selected: selectedReport && (selectedReport.id === report.id),
      },
    }));

  function handleMarkerClick(marker) {
    if (selectedReport && selectedReport.id === marker.metadata.id) {
      setSelectedReport(null);
    } else {
      setSelectedReport(marker.metadata);
    }
  }

  function handleReportSelection(rowdata) {
    const reportData = rowdata;

    // add longitude and latitude to report rowdata
    const locations = getReportsLocations();
    Object.keys(locations).forEach((key) => {
      if (locations[key].metadata.id === reportData.id) {
        reportData.latitude = locations[key].lat;
        reportData.longitude = locations[key].lng;
      }
    });

    if (selectedReport && selectedReport.id === reportData.id) {
      setSelectedReport(null);
    } else {
      setSelectedReport(reportData);
    }
  }

  return (
    <StyledReportsView>

      <SplitView
        className="map_reportsTable"
        direction="column"
        ratios={tableSplitRatio}
        p={mqXs ? 1 : 2}
      >
        <Paper>
          <SplitView
            className="map_details"
            animate={animateResize}
            ratios={detailsSplitRatio}
            viewsRefs={viewsRefs}
          >
            <Map
              mapId="reportsMap"
              zoom={mapProps.zoom}
              center={
                    (!mqXs && selectedReport && selectedReport.latitude && { lat: selectedReport.latitude, lng: selectedReport.longitude })
                    || mapProps.center
                  }
              onMarkerClick={handleMarkerClick}
              locations={getReportsLocations()}
              preventFitBounds
            />
            {selectedReport && (
              <div className="reportDetailsWrapper">
                <IconButton
                  className="closeDetailsBtn"
                  type="button"
                  onClick={() => {
                    setSelectedReport(null);
                  }}
                >
                  <CloseIcon />
                </IconButton>

                {mqXs && (
                <Swipeable
                  delta={5}
                  className="reportDetails"
                  onSwiping={swiping}
                  onSwiped={onSwiped}
                >
                  <ReportDetails report={selectedReport} />
                </Swipeable>
                )}
                {!mqXs && (
                <ReportDetails report={selectedReport} />
                )}
              </div>
            )}
          </SplitView>
        </Paper>
        <Paper>
          <Table
              className="reportsTable"
              tableData={reports.map((report) => (
                {
                  ID: report.id,
                  'Report Time': moment(report.reportTime).format('DD/MM/YYYY HH:mm'),
                  'Received Time': moment(report.receivedTime).format('DD/MM/YYYY HH:mm'),
                  Worker: report.username,
                  Category:  report.category,
                  Text: report.text,
                }
              ))}
              onRowClick={handleReportSelection}
              rowKey="id"
              theme="light"
            />

        </Paper>
      </SplitView>
    </StyledReportsView>
  );
};

ReportsView.displayName = 'ReportsView';

ReportsView.propTypes = {
  
};

ReportsView.defaultProps = {
  
};

export default ReportsView;
