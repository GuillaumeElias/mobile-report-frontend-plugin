import React, { useEffect, useState } from 'react';
import styled from 'styled-components';
import {
  ableRow, TableCell, TableBody, TableRow, Box, Table
} from '@material-ui/core';
import PropTypes from 'prop-types';
import { Spinner } from '@mwp/frontend-components';
import moment from 'moment';

const ReportDetailsBox = styled(Box)`

  display: flex;
  flex-direction: column;
  height: 100%;

  .reportDetailsFooter {
    margin-top: auto;
    display: flex;
    justify-content: space-between;
    align-items: center;
  }

  .buttonsBar {
    text-align: right;
    white-space: nowrap;
  }

  .MuiTableCell-root {
      padding: 0px;
      border-bottom: 0px;
  }

  .labelsColumn{
      width: 100px;
      font-weight: bold;
  }

  .valuesColumn{
    width: 130px;
  }

  .tableWrapper{
    margin-right:40px;
    padding: 2px;
  }
`;

const ReportDetails = ({
  report
}) => {
  /*const userCredentials = useSelector((state) => state.userCredentials);

  const [fetchedReport, setFetchedReport] = useState();
  useEffect(() => {
    if (report.id !== 0) {
      fetchReport(userCredentials, report.id).then(setFetchedReport);
    }
  }, [report, setFetchedReport]);

  const goToReportEvents = () => {
    onReportEvents(report.id);
  };

  if (!fetchedReport) return <Spinner />;

  const formatSelectFieldValue = (string) => {
    const str = string.replace('_', ' ');
    return str.charAt(0).toUpperCase() + str.slice(1).toLowerCase();
  };*/

  return (
    <ReportDetailsBox>
      <Box className="tableWrapper">
        <Table>
          <TableBody>
            <TableRow>
              <TableCell className="labelsColumn">title</TableCell>
              <TableCell className="valuesColumn">
                {report.title}
              </TableCell>
              <TableCell className="labelsColumn">Report Time</TableCell>
              <TableCell className="valuesColumn">
                {moment(report.reportTime).format('DD/MM/YYYY HH:mm')}
              </TableCell>
            </TableRow>
          </TableBody>

        </Table>
      </Box>
    </ReportDetailsBox>
  );
};

ReportDetails.propTypes = {
  report: PropTypes.shape({
    id: PropTypes.string.isRequired,
  }).isRequired,
  onReportEvents: PropTypes.func,
  canEdit: PropTypes.bool.isRequired,
  hideButtons: PropTypes.bool,
};

ReportDetails.defaultProps = {
  hideButtons: false,
  onReportEvents: () => {},
};

export default ReportDetails;
