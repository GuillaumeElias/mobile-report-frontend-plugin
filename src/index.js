import React from "react";
import App from './Page';
import Flag from '@material-ui/icons/Flag';
import {DemoProvider} from "./DemoContext";
import {Widget1, Widget2} from "./Widget";
import wmcSdk from '@mwp/wmc-sdk';
import ReportContainer from "./containers/ReportContainer";

wmcSdk.registerPlugin({
    id: 'mobile-report-frontend-plugin',
    provider: DemoProvider,
    routes: {
        '/reports': ReportContainer
    },
    widgets: [Widget1, Widget2],
    menus: [{
        title: 'Reports',
        path: '/reports',
        icon: <Flag />,
    }]
});
