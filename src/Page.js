import React, {useContext} from 'react';
import {DemoContext, DemoProvider} from "./DemoContext";
import { Box, Button, Paper } from '@material-ui/core';

export default () =>  {
    const { count, increase } = useContext(DemoContext);
    return (
        <Box>
            <Paper m={1} p={2}>
            </Paper>
            <Button variant={"outlined"} onClick={increase}>Increase</Button>
        </Box>
    );
}
