import React, {createContext, useState} from "react";

const DemoContext = createContext({});

const DemoProvider = ({children}) => {
    const [count, setCount] = useState(0);
    const increase = () => setCount((prevCount) => prevCount + 1);
    return (
        <DemoContext.Provider value={
            { count, increase }
        }>
            {children}
        </DemoContext.Provider>
    )
};

export {
    DemoProvider,
    DemoContext,
}